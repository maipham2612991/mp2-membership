package cs425.mp2;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Member extends Thread {
    private boolean isIntroducer;
    private String introHost;
    private int introPort;

    protected AtomicReference<Status> status = new AtomicReference<>(Status.NOT_IN_MEMBERLIST);
    protected MemberInfo memberInfo;
    protected String id;
    protected List<String> memberList = new ArrayList<>();
    protected AtomicBoolean isAck = new AtomicBoolean(false);
    protected String ackRevID = null;
    protected MessageInfo memberMesStatus;
    protected Queue<MessageInfo> operationQueue;
    protected Receiver receiver = null;

    protected static final Logger logger = Logger.getLogger("MP2_Logger");
    protected FileHandler loggerFile ;
//    protected AtomicBoolean terminate = new AtomicBoolean(false);
//    protected int port;

    public Member(int port, boolean isIntroducer, String introHost, int introPort) throws UnknownHostException {
//        this.port = port;
        this.memberInfo = new MemberInfo(port);
        this.id = this.memberInfo.getID();
        this.isIntroducer = isIntroducer;
        this.introHost = introHost;
        this.introPort = introPort;
        this.memberMesStatus = new MessageInfo(MessageType.PING, id, " ");
        this.operationQueue = new LinkedList<>();

        try {
            loggerFile = new FileHandler(Constant.logDir + memberInfo.getPort() + ".log");
            logger.addHandler(loggerFile);
            SimpleFormatter formatter = new SimpleFormatter();
            loggerFile.setFormatter(formatter);
            logger.setUseParentHandlers(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        if (!isIntroducer) {
            logger.info("Member " + memberInfo.getHost() + " started");
        }

        /*
        this function has two responsibilities:
        - Ping random k (=4) members adjacent to it
        - Wait for user command from commandline
         */
        ExecutorService executors = Executors.newFixedThreadPool(3);
        SWIMSend(executors);
        SWIMRev(executors);
        while (true) {
            readCommand();
        }

    }

    /*
    Send thread:
    countToStop: use when dissemination message is sent to all adjacent
    operationQueue: to list all dissemination need to send
    memberMesStatus: current operation (fail, new_join..) in operationQueue, if queue empty, then use ping
     */
    protected void SWIMSend(ExecutorService executors) {
        executors.execute(new Runnable() {
            @Override
            public void run() {
                int countToStop = Math.min((memberList.size() - 1), 3);
//                if (status.get() == Status.NEW_JOIN) {
//                    System.out.println(status.get());
//                }
                while (true) {
//                    if (status.get() == Status.NEW_JOIN) {
//                        System.out.println("debug ");
//                    }

                    if (status.get() == Status.NOT_IN_MEMBERLIST || memberList.size() <= 1) {
                        /*
                        If process has not joined membership list, do nothing
                         */
                        continue;
                    }

                    /*
                    Get random node to ping
                     */

                    Sender sender = new Sender(memberInfo);
                    List<String> pingList = getAdjMember(id);

                    for (String idToPing: pingList) {
                        if (status.get() == Status.NEW_JOIN) {
                        /*
                        If process newly joined membership list, must ping to adj to get the list of membership
                        Caution when pinging fail node
                         */
                            countToStop = Math.min((memberList.size() - 1), 3);
                            operationQueue.add(new MessageInfo(MessageType.NEW_JOIN, id));
                            status.set(Status.IN_MEMBERLIST);
                        }

                        /*
                        Which type of ping to do next
                         */
                        if (operationQueue.isEmpty()) {
                            if (countToStop >= Math.min((memberList.size() - 1), 3)) {
                                memberMesStatus = new MessageInfo(MessageType.PING, id, " ");
                            }
                        } else {
                            if (countToStop >= Math.min((memberList.size() - 1), 3)) {
                                memberMesStatus = operationQueue.remove();
                                countToStop = 0;
                            }
                        }
                        countToStop ++;

                        ackRevID = idToPing;
                        isAck.set(true);
//                        synchronized (isAck) {
//
//                        }
//                        if (memberMesStatus.getMessageType() == MessageType.NEW_JOIN)
//                        System.out.println("debug " + memberMesStatus.getMessageType() + " " + memberMesStatus.getInfo());
                        sender.send(idToPing, memberMesStatus.getMessageType(), memberMesStatus.getInfo());
                        logger.info("Sending from ID=" +memberInfo.getID() + " to ID=" +
                                    idToPing + " with MessageType "+ memberMesStatus.getMessageType() +
                                    " and content " + memberMesStatus.getInfo());

                        long startTime = System.currentTimeMillis(); //fetch starting time
                        while((System.currentTimeMillis()-startTime) <= Constant.waitingTime) {
                            if (!isAck.get()) {
                                logger.info("Wait to receive ACK from ID=" + idToPing);
                                break;
                            }
                        }
                        if (isAck.get()) {
                            operationQueue.add(new MessageInfo(MessageType.LEAVE_FAIL, idToPing));
                            isAck.set(false);
                            logger.info("Fail to receiving ACK from ID=" + idToPing + ". Starting to dissemination");
                        }

//                        try {
//                            Thread.sleep(Constant.SleepTime);
//                        } catch (InterruptedException e) {
//                            throw new RuntimeException(e);
//                        }
                    }

                }
            }
        });
    }

    protected void SWIMRev(ExecutorService executors) {
        executors.execute(new Runnable() {
            public void dissemination(MessageInfo messRev) {
                if (messRev.getMessageType() == MessageType.LEAVE_FAIL && memberList.contains(messRev.getInfo())) {
                    memberList.remove(messRev.getInfo());
                    operationQueue.add(messRev);
                } else if (messRev.getMessageType() == MessageType.NEW_JOIN && !memberList.contains(messRev.getInfo())) {
                    memberList.add(messRev.getInfo());
                    operationQueue.add(messRev);
                }
            }

            @Override
            public void run() {
//                try {
//                    DatagramSocket socket = new DatagramSocket(this.receiverInfo.getPort());
//                } catch (SocketException e) {
//                    e.printStackTrace();
//                }
                while (true) {
                    if (status.get() == Status.NOT_IN_MEMBERLIST || receiver == null) {
                        continue;
                    }

                    MessageInfo messRev = receiver.receive();
                    logger.info("Receiving message from ID=" + messRev.getMemID() +
                                " with messageType " + messRev.getMessageType() +
                                " and content "+messRev.getInfo());
                    if (isAck.get()) {
                        if (messRev.getMemID().equals(ackRevID)) {
                            logger.info("Successfully receive expected ACK from "+ackRevID + ". Staring dissemination");
                            isAck.set(false);
                            dissemination(messRev);
                            continue;
                        }
                    }
                    dissemination(messRev);
                    Sender sender = new Sender(memberInfo);
                    logger.info("Starting to ACK to ID="+messRev.getMemID() +
                                " with messageType " +memberMesStatus.getMessageType() +
                                " and content " + memberMesStatus.getInfo());
                    sender.send(messRev.getMemID(),
                            memberMesStatus.getMessageType(),
                            memberMesStatus.getInfo());
//                    try {
//                        Thread.sleep(Constant.SleepTime);
//                    } catch (InterruptedException e) {
//                        throw new RuntimeException(e);
//                    }
                }
            }
        });
    }

    protected void readCommand() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String command = null;
        try {
            command = reader.readLine();
        } catch (IOException e) {
//            receiver.close();
            System.err.println("Cannot get input from commandline");
        }
        switch (Objects.requireNonNull(command).toLowerCase()) {
            case "join":
                joinMembership();
                break;
            case "leave":
                leaveMembership();
                break;
            case "members":
                printAllMembers();
                break;
            case "id":
                printID();
                break;
            default:
                System.err.println("Cannot found command " + command +". Please enter another command");
                instruction();
        }
    }

    /*
    3 clockwise of given process ID
     */
    protected List<String> getAdjMember(String memberID) {
        int index = memberList.indexOf(memberID);
        int len = memberList.size();

        Set<String> setID = new HashSet<>(
                Arrays.asList(
                    memberList.get((index+1)%len),
                    memberList.get((index+2)%len),
                    memberList.get((index+3)%len)
//                    memberList.get((index+4)%len)
                )
        );
        /*
        Eliminate the chance that a process ping to itself
         */
        setID.remove(this.id);
        ArrayList<String> adjList = new ArrayList<>(setID);
        Collections.shuffle(adjList);

        return adjList;
    }

    protected void joinMembership() {
        /*
        When new process join, connect with introducer by tcp
         */
        if (this.status.get() == Status.IN_MEMBERLIST)
            return;

        this.memberInfo.setTimestamp(System.currentTimeMillis());
        id = this.memberInfo.getID();

        try {
            Socket socket = new Socket(this.introHost, this.introPort);

            /*
            New member send its ID to introducer
             */
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeObject(memberInfo.getID());

            /*
            New member receives member list
             */
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
            String mess = (String) inputStream.readObject();
            String[] memberListStr = mess.trim().split("\n");
            socket.close();

            memberList.addAll(Arrays.asList(memberListStr));
            memberList.add(memberInfo.getID());
            status.set(Status.NEW_JOIN);
            synchronized (status) {
                status.notify();
            }
            receiver = new Receiver(memberInfo);
//            if (receiver == null) {
//
//            } else {
//                receiver.reuseSocket();
//            }

//            terminate.set(false);
//            terminate = new AtomicBoolean(false);

        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Cannot establish TCP connection to Introducer");
        } catch (ClassNotFoundException e) {
            System.err.println("Fail to read message from introducer");
        }

    }

    protected void reset() {
//        receiver.close();
        this.memberMesStatus = new MessageInfo(MessageType.PING, id, " ");
        this.operationQueue = new LinkedList<>();
        this.status = new AtomicReference<>(Status.NOT_IN_MEMBERLIST);
        memberList = new ArrayList<>();
        isAck = new AtomicBoolean(false);
        ackRevID = null;
//        terminate.set(true);
//        terminate = new AtomicBoolean(true);
    }

    protected void leaveMembership() {
        memberList.remove(this.memberInfo.getID());
        reset();
    }

    protected void printAllMembers() {
        System.out.println("List membership: ");
        for (String memId: memberList) {
            MemberInfo meminfo = MemberInfo.fromString(memId);
            System.out.println(meminfo.getHost() + " " + meminfo.getPort());
        }
    }

    protected void printID() {
        System.out.println(this.id);
    }

    protected void instruction() {
        System.out.println("Please input: \n" +
                "join - to join group of membership list \n" +
                "leave - to voluntarily leave group of membership \n" +
                "members - to print all membership list \n" +
                "id - to print ID of the current process \n");
    }

}
